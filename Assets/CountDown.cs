﻿using UnityEngine;
using System.Collections;

public class CountDown : MonoBehaviour {

	public GameObject countDown;
	public CarBehaviour carBehaviour;

	public void OnAnimationFinished()
	{
		countDown.SetActive (false);
		carBehaviour.CanMove(true);
	}
}
