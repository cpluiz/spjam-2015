﻿using UnityEngine;
using System.Collections;
using UnityEditor;
using System.IO;

public class ScriptableObjectUtilitario : MonoBehaviour 
{
	[MenuItem("Window/Criar Config")]
	public static void CriarConfig()
	{
		CriarScriptableObject<CarConfig> ("CarConfig");
	}

	public static void CriarScriptableObject<T>(string nome) 
		where T : ScriptableObject
	{
		T asset = ScriptableObject.CreateInstance<T>();
		
		string diretorio = AssetDatabase.GetAssetPath(Selection.activeObject);
		
		if (diretorio == "")
		{
			diretorio = "Assets";
		}
		else if (Path.GetExtension(diretorio) != "")
		{
			diretorio = diretorio.Replace(Path.GetFileName(AssetDatabase.GetAssetPath(Selection.activeObject)), "");
		}
		
		string diretorioDoAsset = AssetDatabase.GenerateUniqueAssetPath(diretorio + "/" + nome + ".asset");
		
		AssetDatabase.CreateAsset(asset, diretorioDoAsset);
		AssetDatabase.SaveAssets();
		EditorUtility.FocusProjectWindow();
		Selection.activeObject = asset;
	}
}

