﻿using UnityEngine;
using System.Collections;

public class StartRoad : MonoBehaviour {

	public int roadCount;

	void Start(){
		RoadGenerator.ConstructRoad(roadCount);
	}
}
