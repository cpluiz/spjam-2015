﻿using UnityEngine;
using System.Collections;

public class Road : MonoBehaviour {
	public bool drawLine;
	[SerializeField]
	GameObject[] obstacles;
	//Armazena rotação do objeto atual
	public float roadAngle;
	//Armazena posições de início e final da pista atual
	public Transform startPoint, endPoint;
	public Transform[] midPoints;

	void Awake()
	{
		if (obstacles.Length > 0) {
			for (int i = 0; i < obstacles.Length; i++) {
				obstacles [i].SetActive (false);
			}
		}
	}

	public void InitObstacles(){

		if(obstacles.Length > 0){
			for (int i = 0; i < 3; i++)
			{
				if (Random.Range (0, 100) > 65){
					obstacles [Random.Range (0, obstacles.Length)].SetActive (true);
				}
			}

			for (int i = 0; i < obstacles.Length; i++) {
				if (!obstacles [i].activeSelf)
					Destroy (obstacles [i].gameObject);
			}
		}
	}
}
