﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public static class RoadGenerator{

	private static Road[] roadVector;
	private static RoadAngles roadAngles;
	private static double curve;
	public static RoadAngles angles{
		get{
			if(roadAngles == null)
				roadAngles = new RoadAngles ();
			return roadAngles;
		}
	}

	public static UnityEngine.Events.UnityAction<Road[]> onGenerateRoads;

	public static Road[] GetRoads()
	{
		return roadVector;
	}

	public static void ConstructRoad(int tiles){
		DestroyRoad ();
		curve = 0;
		roadVector = new Road[tiles+1];
		roadVector [0] = newRoad (GameObject.FindGameObjectWithTag("StartPoint").transform, 0, 888f);
		for (int i=1; i<tiles; i++) {
			roadVector [i] = newRoad (roadVector [i - 1].endPoint, roadVector [i - 1].roadAngle);

			if (i > 2)
			{
				roadVector [i].InitObstacles();
			}	
		}

		roadVector [tiles] = newRoad (roadVector [tiles-1].endPoint, roadVector [tiles-1].roadAngle, 999f);

		if (onGenerateRoads != null) {
			onGenerateRoads (roadVector);
			onGenerateRoads = null;
		}
			
	}

	public static void DestroyRoad(){
		if (roadVector == null)
			return;
		for (int i=0; i<roadVector.Length; i++)
			GameObject.Destroy (roadVector [i].gameObject);

		roadVector = null;
	}

	//Função que instancia um novo prefab de estrada
	public static Road newRoad(Transform endPosition, float roadAngle, float angle){
		GameObject instantiate =  (GameObject)GameObject.Instantiate(Resources.Load("Prefab/Road/"+angles.GetPrefabName(angle)), endPosition.position, Quaternion.identity);
		Road newRoad = instantiate.GetComponent<Road>();
		newRoad.roadAngle = angle;
		newRoad.transform.rotation = endPosition.transform.rotation;
		return newRoad.GetComponent<Road>();
	}
	public static Road newRoad(Transform endPosition, float roadAngle){
		return newRoad(endPosition,roadAngle,RandomAngle());
	}

	//Função que vai definir aleatoriamente a rotação e o espelhamento da próxima parte da estrada
	public static float RandomAngle(){
		int randomChoice = 0;
		bool validAngle = false;
		while (!validAngle) {
			validAngle = true;
			randomChoice = Random.Range (0, 3);
			if ((curve == -2 && randomChoice == 1) || (curve == 2 && randomChoice == 2))
				validAngle = false;
		}
		switch (randomChoice) {
		case 0:
			return 0f;
			break;
		case 1:
			curve -= 1;
			return -45f;
			break;
		case 2:
			curve += 1;
			return 45f;
			break;
		default:
			return 0f;
		}
	}
}

public class RoadAngles{
	private Dictionary<float,string> prefabNames;

	public RoadAngles(){
		prefabNames = new Dictionary<float, string> ();
		prefabNames.Add (0f, "road1");
		prefabNames.Add (-45f, "road2");
		prefabNames.Add (45f, "road3");
		prefabNames.Add (999f, "victory");
		prefabNames.Add (888f, "start");
	}
	public string GetPrefabName(float angle){
		string angleName = "road1";
		prefabNames.TryGetValue (angle, out angleName);
		return angleName;
	}
}