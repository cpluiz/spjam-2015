﻿using UnityEngine;
using System.Collections;
using UnityEngine.Events;

public class ItemBehaviour : MonoBehaviour 
{
	public int scoreValue;
	public Rigidbody itemRigidbody;

	public void SetKinematic(bool isKinematic)
	{
		itemRigidbody.isKinematic = isKinematic;
	}
}
