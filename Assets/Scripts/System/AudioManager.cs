﻿using UnityEngine;
using UnityEngine.Audio;
using System.Collections;

public class AudioManager : MonoBehaviour
{
	[SerializeField]
	private AudioSource musicSource,effectsSource,uiSource;
	// Use this for initialization
	void Start ()
	{
		if (musicSource == null)
			musicSource = InitSource ("MusicSource");
		if (effectsSource == null)
			effectsSource = InitSource ("EffectsSource");
		if (uiSource == null)
			uiSource = InitSource ("UiSource");
		musicSource.loop = true;
		musicSource.volume = 1;
		musicSource.clip = Resources.Load<AudioClip>("Audio/Music/IngameMusic");
		musicSource.Play();
	}

	private AudioSource InitSource(string name){
		GameObject source = new GameObject();
		source.transform.parent = this.transform;
		source.name = name;
		source.AddComponent<AudioSource>();
		return source.GetComponent<AudioSource>();
	}

	public void AudioVolume(float volume){
		if (musicSource == null)
			Start ();
		musicSource.volume = volume/100;
	}

	public void EffectsVolume(float volume){
		effectsSource.volume = volume/100;
		uiSource.volume = effectsSource.volume;
	}

	public void PlayUiEffect(string effectName){
		uiSource.PlayOneShot(Resources.Load<AudioClip>("Audio/Effects/UI/"+effectName));
	}

	public void PlayEffect(string effectName){
		effectsSource.PlayOneShot(Resources.Load<AudioClip>("Audio/Effects/"+effectName));
	}
}

