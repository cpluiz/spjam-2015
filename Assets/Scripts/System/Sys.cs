﻿using UnityEngine;
using System.Collections;

public static class Sys {
	private static InterfaceManager uiManager;
	private static AudioManager audioManager;
	public static InterfaceManager interfaceManager{
		get{
			if (uiManager == null)
				uiManager = GameObject.FindObjectOfType<InterfaceManager> ();
			return uiManager;
		}
	}
	public static AudioManager soundManager{
		get{
			if (audioManager == null) {
				Camera.main.gameObject.AddComponent<AudioManager> ();
				audioManager = Camera.main.GetComponent<AudioManager> ();
			}
			return audioManager;
		}
	}

	public static GameType gameType;

	public static void Pause(){
		if (Time.timeScale == 1) {
			//interfaceManager.showInterface(ui.pauseMenu);
			Time.timeScale = 0;
		}else{
			//interfaceManager.closeInterface(true);
			Time.timeScale = 1;
		}
	}
}

public enum GameType{SinglePlayer,MultiPlayer}
