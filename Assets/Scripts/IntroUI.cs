﻿using UnityEngine;
using System.Collections;

public class IntroUI : MonoBehaviour 
{
	public GameObject playMenu;
	public GameObject creditsMenu;

	public void OnButtonPlay()
	{
		Application.LoadLevel ("GAME");
	}

	public void OnButtonCredits()
	{
		playMenu.SetActive (false);
		creditsMenu.SetActive (true);
	}

	public void OnButtonMenu()
	{
		playMenu.SetActive (true);
		creditsMenu.SetActive (false);
	}
}
