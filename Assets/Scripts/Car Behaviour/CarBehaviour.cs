﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class CarBehaviour : MonoBehaviour
{
	public enum PlayerType
	{
		PLAYER_1,
		PLAYER_2,
		SINGLEPLAYER,
		MULTIPLAYER_CLIENT,
		MULTIPLAYER_SERVER,
		PC
	}

	public PlayerType playerType;
	public Rigidbody carRigidbody;
	public float horizontalMaxSpeed;
	public float maxVerticalSpeed;
	public float verticalSpeed;
	public float rotationSpeed;
	public float rotationZLimit;
	public float wheelRotationSpeed;
	public Transform[] frontWheels;
	public Transform[] backWheels;
	public Transform cachedTransform;
	private float horizontalAxis;
	private float verticalAxis;
	private float horizontalAxisSum;
	private float distanceToNextTransform;
	public Renderer[] carRenderers;
	public bool canMove;
	public CarBucket carBucket;
	public CameraScript carCamera;
	public GameObject[] onMoveGameObjects;

	public GameObject youWin;
	public Text textWinScore;

	public enum PlayerState
	{
		RUN,
		WIN
	}

	public Animator carAnimator;
	public PlayerState playerState;

	public void OnWin() 
	{
		carAnimator.CrossFade("Trailer_WIN", 0f);

		carBucket.SetFreezeScore (true);

		carCamera.OnWin ();

		youWin.SetActive (true);
		textWinScore.text = carBucket.scoreText.text;
	}

	public void CanMove(bool can)
	{
		canMove = can;
		carRigidbody.isKinematic = !can;

		if (playerState != PlayerState.WIN) {
			carBucket.SetItemKinematics(!can);
		}

		for (int i = onMoveGameObjects.Length; i --> 0;) {
			onMoveGameObjects[i].SetActive(can);
		}
	}

	void Start ()
	{
		currentPointIndex = 0;
		currentRoadIndex = 0;
		currentPointTransform = null;
		playerState = PlayerState.RUN;

		Sys.gameType = GameType.MultiPlayer;
		if (Sys.gameType == GameType.SinglePlayer && (this.playerType == PlayerType.PLAYER_1 || this.playerType == PlayerType.PLAYER_2))
			this.gameObject.SetActive (false);
		if (Sys.gameType == GameType.MultiPlayer && this.playerType == PlayerType.SINGLEPLAYER)
			this.gameObject.SetActive (false);
			
		cachedTransform = transform;

		Road[] roads = RoadGenerator.GetRoads ();

		if (roads == null) {
			RoadGenerator.onGenerateRoads = OnGenerateRoads;
		} else {
			OnGenerateRoads (roads);
		}
	}

	void LoadMaterials()
	{
		CarConfig config = CarConfig.Get ();
		
		if (playerType == PlayerType.PLAYER_1) {
			SetRendererMaterials (config.trailerMaterials [0]);
		} else if (playerType == PlayerType.PLAYER_2) {
			SetRendererMaterials (config.trailerMaterials [1]);
		} else if (playerType == PlayerType.MULTIPLAYER_SERVER) {
			SetRendererMaterials (config.trailerMaterials [Random.Range (0, config.trailerMaterials.Length)]);
		}
	}

	public void SetRendererMaterials (Material mat)
	{
		for (int i = carRenderers.Length; i --> 0;) {
			carRenderers [i].material = mat;
		}
	}

	void FixedUpdate ()
	{
		if (roads != null && canMove) {
			Move ();
			AutoMove ();
			Rotate ();
			//RotateWheels ();
		}
	}

	void Move ()
	{
		#if UNITY_ANDROID

		horizontalAxis = -Input.acceleration.x * 2f;
		//verticalAxis = Input.touches.Length > 0 ? 1f : 0f;

		#endif
		
		#if UNITY_STANDALONE || UNITY_EDITOR

		if (this.playerType == PlayerType.PLAYER_1 || this.playerType == PlayerType.SINGLEPLAYER)
			horizontalAxis = Input.GetAxis ("Horizontal");
		if(this.playerType==PlayerType.PLAYER_2)
			horizontalAxis = Input.GetAxis ("Horizontal2");
		//verticalAxis = Input.GetAxis ("Vertical");

		#endif

		verticalAxis = 1f;
		//verticalAxis = Mathf.Lerp(verticalAxis, Input.GetAxis("Vertical"), Time.deltaTime);
	}

	void MoveTranslate ()
	{
		cachedTransform.Translate (0, 0, horizontalAxis, Space.Self);
	}

	private RaycastHit raycastHit;

	bool IsOnGround ()
	{
		return true;
	}

	void AutoMove ()
	{
		cachedTransform.position = Vector3.MoveTowards (cachedTransform.position, currentPointTransform.position, verticalAxis * verticalSpeed * Time.deltaTime);
		cachedTransform.Translate (0, 0, horizontalAxis * verticalAxis * horizontalMaxSpeed * Time.deltaTime, Space.Self);
		distanceToNextTransform = Vector3.Distance (cachedTransform.position, currentPointTransform.position);

		if (distanceToNextTransform < 5f) {
			if (HasNextRoadIndex ()) {
				currentPointTransform = GetNextTransform ();
			} else {
				if (currentRoadIndex > 0)
				{
					playerState = PlayerState.WIN;
					OnWin();
				}

				CanMove(false);
			}
		}
	}

	void Rotate ()
	{
		horizontalAxisSum += -horizontalAxis;
		horizontalAxisSum = Mathf.Clamp (horizontalAxisSum, -rotationZLimit, rotationZLimit);

		if (horizontalAxisSum < rotationZLimit && horizontalAxisSum > -rotationZLimit && Mathf.Abs (horizontalAxis) != 0) {
			transform.Rotate (horizontalAxis * rotationSpeed, 0, 0);

		} else if (transform.rotation.x < 0f && horizontalAxis < 0) {
			transform.Rotate (.2f * rotationSpeed, 0, 0);

		} else if (transform.rotation.x > 0f && horizontalAxis > 0) {
			transform.Rotate (-.2f * rotationSpeed, 0, 0);
		}

		if (transform.rotation.x > 0.1f && transform.rotation.x < 0.1f) {
			horizontalAxisSum = 0f;
		}

		cachedTransform.rotation = Quaternion.Slerp (cachedTransform.rotation, currentPointTransform.rotation, Time.deltaTime);
	}

	void RotateWheels ()
	{
		for (int f = frontWheels.Length; f --> 0;) {
			frontWheels [f].Rotate (0, carRigidbody.velocity.magnitude * verticalAxis * wheelRotationSpeed, 0);
		}
		
		for (int b = backWheels.Length; b --> 0;) {
			backWheels [b].Rotate (0, carRigidbody.velocity.magnitude * verticalAxis * wheelRotationSpeed, 0);
		}
	}

	private Road[] roads;
	private int currentRoadIndex;
	private int currentPointIndex;
	private Transform currentPointTransform;

	void OnGenerateRoads (Road[] roads)
	{
		this.roads = roads;
		currentPointTransform = GetNextTransform ();
		currentPointTransform = GetNextTransform ();

		transform.position = currentPointTransform.position;
	}

	bool HasNextRoadIndex ()
	{
		return currentRoadIndex < roads.Length;
	}

	Transform GetNextTransform ()
	{
		Road road = roads [currentRoadIndex];

		int length = 2 + road.midPoints.Length;

		if (currentPointIndex == 0) {

			currentPointIndex++;
			return road.startPoint;

		} else if (currentPointIndex == length - 1) {

			currentPointIndex = 0;
			currentRoadIndex++;
			return road.endPoint;

		} else {
			return road.midPoints [++currentPointIndex - 2];
		}
	}
}
