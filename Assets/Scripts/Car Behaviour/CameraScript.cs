﻿using UnityEngine;
using System.Collections;

public class CameraScript : MonoBehaviour
{
	public Transform target;
	public float smooth = 0.3F;
	public float distance = 5.0F;
	private float yVelocity = 0.0F;

	public Transform onWin;
	public bool isWin;

	public void OnWin()
	{
		isWin = true;
	}

	void LateUpdate () {
		// Early out if we don't have a target
		if (!target)
			return;

		if (isWin) {
			transform.localPosition = Vector3.Lerp(transform.localPosition, onWin.localPosition, Time.deltaTime);
		}

		float yAngle = Mathf.SmoothDampAngle(transform.eulerAngles.x, target.eulerAngles.x, ref yVelocity, smooth);
		Vector3 position = transform.position;
		transform.position = position;
		position += Quaternion.Euler(0, yAngle, yAngle) * new Vector3(0, 0, -distance);
		transform.eulerAngles = position;
		// Always look at the target
		transform.LookAt (target);
	}
}