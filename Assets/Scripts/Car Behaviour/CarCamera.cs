﻿using UnityEngine;
using System.Collections;

public class CarCamera : MonoBehaviour 
{
	public CarBehaviour carBehaviour;

	private Transform cachedTransform;
	private Vector3 currentVelocity;

	public Vector3 offsetPosition;
	public Vector3 offsetRotation;

	void Awake()
	{
		cachedTransform = transform;
	}

	float yVelocity;
	public float smooth;
	public float distance;

	void LateUpdate()
	{
		float yAngle = Mathf.SmoothDampAngle(cachedTransform.eulerAngles.y, carBehaviour.cachedTransform.eulerAngles.y, ref yVelocity, smooth);
		cachedTransform.rotation = Quaternion.Euler(0, yAngle, 0);

		Vector3 positionTarget = new Vector3 (carBehaviour.cachedTransform.position.x + offsetPosition.x, 
				                              carBehaviour.cachedTransform.position.y + offsetPosition.y, 
				                              carBehaviour.cachedTransform.position.z + offsetPosition.z);

		cachedTransform.position = Vector3.MoveTowards (cachedTransform.position, positionTarget, Time.deltaTime * 100f);

		//cachedTransform.position = Vector3.SmoothDamp (cachedTransform.position, positionTarget, ref currentVelocity, Time.deltaTime);

		Vector3 rotationTarget = new Vector3 (carBehaviour.cachedTransform.position.x + offsetRotation.x, 
		                                      carBehaviour.cachedTransform.position.y + offsetRotation.y, 
		                                      carBehaviour.cachedTransform.position.z + offsetRotation.z);



		//cachedTransform.LookAt (carBehaviour.cachedTransform.position);
	}
}
