﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;

public class CarBucket : MonoBehaviour 
{
	public Text scoreText;
	public int score;
	public CarBehaviour carBehaviour;
	public List<ItemBehaviour> items;

	private bool freezeScore;

	public void SetFreezeScore (bool freeze)
	{
		freezeScore = freeze;
	}

	public void SetItemKinematics(bool isKinematic)
	{
		for (int i = items.Count; i --> 0;) {
			items[i].SetKinematic(isKinematic);
		}
	}

	void Awake ()
	{
		CarConfig config = CarConfig.Get ();
		
		Transform itemsPrefab = Instantiate(config.prefabsItems[Random.Range (0, config.prefabsItems.Length)]).transform;
		itemsPrefab.parent = transform.parent;
	}

	void Start () 
	{
		CheckScore ();
	}

	void OnTriggerEnter (Collider collision)
	{
		if (freezeScore)
			return;

		ItemBehaviour itemBehaviour = collision.GetComponent<ItemBehaviour> ();

		if (itemBehaviour != null) {
			bool hasItem = false;
			
			for (int i = items.Count; i --> 0;) {
				
				ItemBehaviour otherItem = items[i];
				
				if (otherItem != null && itemBehaviour.name == otherItem.name)
				{
					hasItem = true;
					break;
				}
			}
			
			if (!hasItem) {
				AddScore(itemBehaviour.scoreValue);
				items.Add(itemBehaviour);
			}
		}
	}

	void OnTriggerExit (Collider collision)
	{
		if (freezeScore)
			return;

		ItemBehaviour itemBehaviour = collision.GetComponent<ItemBehaviour> ();

		if (itemBehaviour != null) {
			for (int i = items.Count; i --> 0;) {
				
				ItemBehaviour otherItem = items[i];
				
				if (otherItem != null && itemBehaviour.name == otherItem.name)
				{
					RemoveScore(itemBehaviour.scoreValue);
					ItemToThrash(itemBehaviour);
					items.RemoveAt(i);
					break;
				}
			}
		}
	}

	public void AddScore(int value)
	{
		score += value;
		scoreText.text = score.ToString();
	}

	public void RemoveScore(int value)
	{
		score -= value;
		scoreText.text = score.ToString();
	}

	public void CheckScore()
	{
		for (int i = items.Count; i --> 0;) {
			score += items[i].scoreValue;
		}

		scoreText.text = score.ToString();
	}

	public void ItemToThrash(ItemBehaviour item)
	{
		item.transform.parent = null;
		StartCoroutine(DestroyItemAfter (item, 15f));
	}

	IEnumerator DestroyItemAfter(ItemBehaviour item, float seconds)
	{
		yield return new WaitForSeconds(seconds);

		if (item != null)
			Destroy (item.gameObject);
	}
}
