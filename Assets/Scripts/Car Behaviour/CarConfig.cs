﻿using UnityEngine;
using System.Collections;

[System.Serializable]
public class CarConfig : ScriptableObject 
{
	public GameObject[] prefabsItems;
	public Material[] trailerMaterials;

	private static CarConfig instance;

	public static CarConfig Get()
	{
		if (instance != null) {
			return instance;
		} else {
			instance = Resources.Load<CarConfig> ("CarConfig");
			return instance;
		}
	}
}
